package Logic;

import java.util.ArrayList;

import GUI.Key;
import GUI.Screen;

public class Cont {

	private static Screen scr;
	private static ArrayList<Key> moving;
	public static final int SIZE = 50;
	
	//set up the x and y positions for the Catch keys
	public static final int[] CatchX = new int[] { SIZE*0, SIZE*1, SIZE*2, SIZE*3 };
	public static final int[] CatchY = new int[] { SIZE,   SIZE,   SIZE,   SIZE };
	
	//set up the x and y positions for the moving keys 
	public static final int[] ThrowX = new int[] { SIZE*0, SIZE*1, SIZE*2, SIZE*3 };
	public static final int[] ThrowY = new int[] { SIZE*9, SIZE*9, SIZE*9, SIZE*9 };
	
	//Array of keycodes that is used by the checkPress
	public static final int[] keyCodes = new int[] {37,38,40,39}; 
	private static Key[] catchers = new Key[4];
	
	//Returns: the current screen
	public static Screen getScreen(){
		return Cont.scr;
	}
	
	//Parameter: int k
	//Returns: index of k within the keyCodes array
	public static int getKeyCodeIndex(int k){
		for(int i = 0;i<keyCodes.length;i++){
			if(k==keyCodes[i])
				return i;
		}
		return -1;
	}
	
	//Parameter: Screen scr
	//Sets the current screen to scr
	public static void setScreen( Screen scr){
		Cont.scr = scr;
	}
	
	//Parameter: Key k
	//adds the passed in key to the ArrayList of moving keys
	public static void addMoving(Key k){
		moving.add(k);
	}
	
	//Returns: ArrayList of moving keys
	public static ArrayList<Key> getMoving(){
		return Cont.moving;
	}
	
	//Returns: the keys that are seen on the current screen
	public static ArrayList<Key> getOnScreen(){
		ArrayList<Key> OnScreen = new ArrayList<Key>();
		for(Key k : Cont.moving){
			if(k.getY() > 0 && k.getY() < Cont.SIZE*9)
				OnScreen.add(k);
		}
		return OnScreen;
	}
	
	//Constructor: sets up the moving ArrayList
	public Cont(){
		moving = new ArrayList<Key>();
	}
	
	//Returns: catchers Array
	//might not be used anywhere - make sure to delete
	public static Key[] getCatchers() {
		return catchers;
	}
}