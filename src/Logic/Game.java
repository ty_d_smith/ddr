package Logic;

import java.awt.Color;
import java.util.Random;

import GUI.Key;
import GUI.Moving;
import GUI.Screen;

public class Game{
	
	private static Cont cont;
	private static Screen scr;
	
	//all 4 of these are used for timing purposes
	private static long startPaint;
	private static long currentPaint;
	private static long startAdd;
	private static long currentAdd;
	
	//Overall score of the game
	private static int score; 
	
	//Used by the checkZone method
	private static Key target;
	private static int height;
	
	//Main: starts the game
	public static void main (String [] args){
		cont = new Cont();
		scr = new Screen(cont);
		Cont.setScreen(scr);
		play();
	}
	
	//Main game method
	//Continuously runs and sets the background color
	//		-along with setting the timing for the keys to rise
	public static void play(){
		score = 0;
		new Moving(cont);
		startPaint = System.currentTimeMillis();
		startAdd = System.currentTimeMillis();
		Random rand = new Random();
		while(true){
			int R = rand.nextInt(255);
			int G = rand.nextInt(255);
			int B = rand.nextInt(255);
			currentPaint = System.currentTimeMillis();
			currentAdd = System.currentTimeMillis();
			if( currentPaint-startPaint > 10 ){
				startPaint = System.currentTimeMillis();
				for( Key k : Cont.getMoving() ){	
					k.setPos(k.getX(), k.getY()-(Cont.SIZE/16));
				}
				scr.repaint();
			}

			if( currentAdd-startAdd > 600 ){
				startAdd = System.currentTimeMillis();
				new Moving(cont);
				scr.setBackground(new Color(R,G,B));
			}	
		}
	}
	
	//Calls the checkZone method
	//Sent here from the keyPress register
	public static void checkKeyPress(int keyCode) {
		checkZone(keyCode);
	}
	
	public static void checkKeyRelease(int keyCode) {}
	
	//Debug purposes
	public static String printDir(int n){
		if(n==37) return "left";
		else if(n==38) return "up";
		else if(n==39) return "right";
		else return "down";
	}

	//Parameter: int keyCode
	//Sets the uppermost key to the target then checks:
	//	a - if the keyCodes match
	//  b - if the zones match when the key is pressed
	//Also updates the score
	public static void checkZone( int keyCode ){
		target = null;
		height = Cont.SIZE*9;
		for(Key k : Cont.getOnScreen() ){
			if( k.getY() < height && k.getY() >= 25 ){
				target = k;
				height = k.getY();
			} 
		}
		
		if( Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]+5 > height && 
				height > Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]-5 && target.getkeyCode()==keyCode ){
			score+=10;
			System.out.println( "Perfect     " + score);
			target.setVis(false);
			target = null;
		}else if( Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]-12 < height && 
				height < Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]+12 && target.getkeyCode()==keyCode){
			score+=5;
			System.out.println( "Great     " + score);
			target.setVis(false);
			target = null;
		}else if( Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]+25 > height && 
				height > Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]-25 && target.getkeyCode()==keyCode ){
			score+=2;
			System.out.println( "Okay     " + score);
			target.setVis(false);
			target = null;
		}else if( target.getkeyCode() != keyCode ){
			System.out.print("wrong key");
			System.out.println("   Target: " + target.getName() + "      you hit: " + printDir(keyCode) +"  "+ Cont.getOnScreen());
			target = null;
		}else{
			System.out.println("miss");
			target = null;
		}
	}
}