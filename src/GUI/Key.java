package GUI;

import java.awt.Dimension;

import javax.swing.JButton;

import Logic.Cont;

@SuppressWarnings("serial")
public class Key extends JButton{

	protected int direction;
	private boolean catcher = false;
	private boolean keyOn = false;
	protected int keyCode;
	
	//Constructor: sets the dimensions for each key
	public Key(Cont cont){
		super();
		this.setSize( new Dimension(Cont.SIZE, Cont.SIZE) );		
	}
	
	//Returns: keycode
	public int getkeyCode(){
		return keyCode;
	}
	
	//not sure exactly - might not be used
	public void setDirection(int dir){
		this.direction = dir;
	}
	
	//Params: int x, int y
	//Sets the location of this key
	public void setPos( int x, int y ){
		this.setLocation( x, y );
	}
	
	//Parameter: boolean b
	//Sets the visibility of this key
	public void setVis( boolean b ){
		this.setVisible( b );
	}
	
	//dont think this is used 
	public void setCatcher( boolean b ){
		this.catcher = b;
	}
	//dont think this is used either
	public void keyOn( boolean b ){
		this.keyOn = b;
	}
	
	//Returns: the name of the key for debug purposes
	public String toString(){
		if(keyCode==37) return "left";
		if(keyCode==38) return "up";
		if(keyCode==39) return "right";
		if(keyCode==40) return "down";
		return "";
	}
}