package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Logic.Cont;
import Logic.Game;

@SuppressWarnings("serial")
public class Screen extends JPanel implements KeyListener{

	private JFrame frame;
	private static Image Pic;
	
	//Constructor: sets up the screen
	public Screen(Cont cont){
		super();
		this.setPreferredSize( new Dimension( Cont.SIZE*4, Cont.SIZE*9 ) );
		this.setBackground( Color.black );
		this.setLayout( null );
		addKeyListener(this);
		setFocusable(true);
		choosePic(cont);
		frame = new JFrame();
		frame.setVisible( true );
		frame.setResizable( false );
		frame.setLocation( 500, 100 );
		frame.add( this );
		frame.addKeyListener( this );
		frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		frame.addWindowListener( new WindowAdapter(){
			public void windowClosing( WindowEvent w ){
				System.exit( 0 );
			}
		} );
		frame.pack();
	}

	//Sets up the catch keys for the constructor
	public void choosePic(Cont cont){
		for(int i=0; i<4; i++){
			Key k = new Key( cont );
			k.setPos( Cont.CatchX[ i ], Cont.CatchY[ i ] );
			if(i==0){
				Pic = Toolkit.getDefaultToolkit().getImage("left.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==1){
				Pic = Toolkit.getDefaultToolkit().getImage("up.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==2){
				Pic = Toolkit.getDefaultToolkit().getImage("down.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==3){
				Pic = Toolkit.getDefaultToolkit().getImage("right.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			this.add( k );
			//Cont.getCatchers()[ i ] = k;
		}
	}

	//This is how the key presses get registered
	@Override
	public void keyPressed(KeyEvent e) {
		Game.checkKeyPress( e.getKeyCode() );
	}
	@Override
	public void keyReleased(KeyEvent e) {
		//Game.checkKeyRelease( e.getKeyCode() );
	}
	@Override
	public void keyTyped(KeyEvent e) {}
}