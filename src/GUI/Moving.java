package GUI;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.Random;

import javax.swing.ImageIcon;

import Logic.Cont;

@SuppressWarnings("serial")
public class Moving extends Key{
	
	private int index;
	private Image Pic;
	private static String name;

	//Constructor: sets up the throw keys 
	public Moving(Cont cont){
		super(cont);
		Random rand = new Random();
		this.index = rand.nextInt(4); 
		setPos(Cont.ThrowX[index], Cont.ThrowY[index]);
		setPic(index);
		Cont.getScreen().add(this);
		Cont.addMoving( this );
		this.setCatcher( false );
	}
	
	//Returns: name of the key - debug purposes
	public String getName(){
		return name;
	}
	
	//Sets the images on the throw keys
	public void setPic(int index){
		if(index == 0){
			name = "left";
			Pic = Toolkit.getDefaultToolkit().getImage("left.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 37; 
		}
		if(index == 1){
			name = "up";
			Pic = Toolkit.getDefaultToolkit().getImage("up.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 38;
		}
		if(index == 2){
			name = "down";
			Pic = Toolkit.getDefaultToolkit().getImage("down.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 40;
		}
		if(index == 3){
			name = "right";
			Pic = Toolkit.getDefaultToolkit().getImage("right.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 39;
		}
	}
}